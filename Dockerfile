FROM golang:alpine3.12

ADD ./crawler /go/src/crawler
WORKDIR /go/src/crawler

RUN \
       apk add --no-cache bash git openssh && \
       # go mod init github.com/Gustibimo/telunjuktest && \
       go build && \
       go install

CMD ["go","run","main.go"]
package main

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/gocolly/colly"
)

func main() {

	start := time.Now()

	categories := make([]string, 0, 500)

	collector1 := colly.NewCollector(
		colly.Async(true))

	collector1.OnHTML("li", func(ec *colly.HTMLElement) {
		category := ec.ChildAttr("a", "data-url")
		categories = append(categories, category)

	})

	collector1.Visit("https://orami.co.id")
	collector1.Wait()

	fileOutput := "output/orami_products.csv"
	file, err := os.Create(fileOutput)
	if err != nil {
		log.Fatalf("Cannot create file %q: %s\n", fileOutput, err)
		return
	}
	defer file.Close()
	writer := csv.NewWriter(file)
	defer writer.Flush()
	// Write CSV header
	writer.Write([]string{"Name", "URL"})

	// Instantiate default collector
	c := colly.NewCollector(
		// Allow requests only to store.xkcd.com
		colly.Async(true),
		colly.AllowedDomains("www.orami.co.id"),
	)
	// c.Limit(&colly.LimitRule{DomainGlob: "*", Parallelism: 2})

	// Extract product details
	c.OnHTML("div[class*='prod-name mb-8']", func(e *colly.HTMLElement) {
		writer.Write([]string{
			e.ChildText("a"),
			e.ChildAttr("a", "href"),
		})
	})

	// Find and visit next page links
	c.OnHTML(`.paging a[href]`, func(e *colly.HTMLElement) {
		e.Request.Visit(e.Attr("href"))
	})

	var targetURL string = "https://www.orami.co.id/"

	for _, url := range categories {
		if err != nil {
			log.Fatal(err)
		}
		c.Visit(targetURL + url)
	}

	log.Printf("Scraping finished, check file %q for results\n", fileOutput)
	log.Println(c)
	c.Wait()

	elapsed := time.Since(start)
	fmt.Printf("Process took %s ", elapsed)
}

# Telunjuk Test

## Prerequisites

- Docker
- Go

## Setup

- First, clone the repository into your local machine and go to `telunjuktest` folder.

<pre>
> git clone https://gitlab.com/Gustibimo/telunjuktest
> cd telunjuktest
</pre>

## Run

Run the crawler by execute `docker-compose` command

<pre>
> docker-compose up

[output]

> Starting crawler ... done
Attaching to crawler
crawler    | 2020/07/21 02:46:17 Scraping finished, check file "output/orami_products.csv" for results
crawler    | 2020/07/21 02:46:17 Requests made: 95 (0 responses) | Callbacks: OnRequest: 0, OnHTML: 2, OnResponse: 0, OnError: 0
crawler    | Process took 18.180792988s crawler
</pre>

Pleas make sure the process took a requests, if it's `Requests made: 0`, please try to execute `docker-compose up` again. The result CSV file will be in folder `output`.